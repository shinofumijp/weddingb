class CreateForms < ActiveRecord::Migration[5.2]
  def change
    create_table :forms do |t|
      t.boolean :attendance, default: true
      t.string :familyname
      t.string :firstname
      t.integer :postalcode
      t.string :address
      t.integer :phone
      t.string :email
      t.string :allergy
      t.text :message
      t.timestamps
    end
  end
end
